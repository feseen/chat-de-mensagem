var messages = []

function showMessageBox(){

    //Criação da caixa de texto
    let newMessage = document.createElement("textarea")
    newMessage.value = messages.at(-1)
    newMessage.readOnly = true

    //Criação do botão "Editar"
    let editButton = document.createElement("button")
    editButton.setAttribute("class", "edit")
    editButton.innerHTML = "Editar"

    //Criação do botão "Excluir"
    let deleteButton = document.createElement("button")
    deleteButton.setAttribute("class", "delete")
    deleteButton.innerHTML = "Excluir"

    //Criação da div que engloba os elementos anteriores criados dinamicamente
    let messageBox = document.createElement("div")
    messageBox.setAttribute("class", "message-box")
    messageBox.appendChild(newMessage)
    messageBox.appendChild(editButton)
    messageBox.appendChild(deleteButton)

    //Inserção da nova caixa de mensagem ao DOM
    document.getElementById("history").appendChild(messageBox);   
}

let sendButton = document.querySelector("#send")

sendButton.addEventListener("click", () => {
    let inputText = document.getElementById("new-message")
    messages.push(inputText.value)
    inputText.value = ""
    showMessageBox()
})

document.addEventListener('click', (btn) => {
    if(btn.target.getAttribute('class') == 'edit'){
        let textArea = btn.target.previousElementSibling
        if(textArea.readOnly){
            textArea.style.backgroundColor = "#efa766"
            messages.splice(messages.indexOf(textArea.value), 1)
            textArea.readOnly = false
        }
        else{
            textArea.style.backgroundColor = "white"
            messages.push(textArea.value)
            textArea.readOnly = true
        }
    }
})

document.addEventListener('click', (btn) => {
    if(btn.target.getAttribute('class') == 'delete'){
        messages.splice(messages.indexOf(btn.target.previousElementSibling.previousElementSibling.value), 1)
        btn.target.parentElement.style.display = "none"
    }
})
